const dueSmsCounter = require('../distribution/index');

// tests factorizations functions
expectedResult = (obj, exlude_keys) => {
  let result = {...obj};
  delete result.string;
  delete result.desc;
  delete result.max_sms;
  (exlude_keys || []).forEach(k => delete result[k]);
  return result;
};

testCountObj = obj => {
  return expect(dueSmsCounter.calcCharCount(obj.string)).toEqual(expectedResult(obj, ['sms_count']));
}

runCountTest = obj => {
  it(obj.desc, function() {
    testCountObj(obj);
  });
}

testSmsCountObj = obj => {
  return expect(dueSmsCounter.calcCharAndSmsCount(obj.string)).toEqual(expectedResult(obj));
}

runSmsCountTest = obj => {
  it(obj.desc, function() {
    testSmsCountObj(obj);
  });
}

testMaxCharObj = obj => {
  return expect(dueSmsCounter.calcMaxCharCount(obj.max_sms)).toEqual(expectedResult(obj));
}

runMaxCharTest = obj => {
  it(obj.desc, function() {
    testMaxCharObj(obj);
  });
}

// tested contents
// sms char counts tests
const emptySms = {
  desc: "returns 0 and not unicode if sms is empty",
  string: "",
  is_unicode: false,
  char_count: 0,
  sms_count: 0
};

const gsm7With2SizedChars = {
  desc: "returns more than char.count and not unicode if sms is has only gsm7 chars and some 2sized chars",
  string: "|o|",
  is_unicode: false,
  char_count: 5,
  sms_count: 1
}

const gsm7Without2SizedChars = {
  desc: "return char.count and not unicode",
  string: "this is my tested string, even if its a very very very very very very very very very very very very very very very very very very very very very very long one, this should works perfectly (size = 200)",
  is_unicode: false,
  char_count: 200,
  sms_count: 2
}

const unicodeSms = {
  desc: "return is unicode with right char.count",
  string: "unicºde string with some gsm72sized chars => |",
  is_unicode: true,
  char_count: 46,
  sms_count: 1
}

const twoSizedGsm7Sms = {
  desc: 'gsm7 sms in 2 sms',
  string: 'this is an gsm7 |o| sms with more char than for one sms but which can be send with 2 sms. So its long long long long long long long long long long long long long',
  is_unicode: false,
  char_count: 163,
  sms_count: 2
}

const fourSizedGsm7Sms = {
  desc: 'unicode sms in 4 sms',
  string: 'this is ˙´®oiu¯ßð an gsm7 |o| sms with more char than for one sms but which can be send with 2 sms. So its long long long long long long long long long long long long long',
  is_unicode: true,
  char_count: 171,
  sms_count: 3
}

// max sms char tests
const emptyCount = {
  desc: "max_sms = 0 should return empty 0 for both",
  max_sms: 0,
  gsm7: 0,
  unicode: 0
}

const oneSmsCount = {
  desc: "max_sms = 1 should return empty 160(gsm7) and 70(unicode)",
  max_sms: 1,
  gsm7: 160,
  unicode: 70
}

const tenCount = {
  desc: "max_sms = 10 should return empty 1520(gsm7) and 660(unicode)",
  max_sms: 10,
  gsm7: 1520,
  unicode: 660
}

const overSizedCount = {
  desc: "max_sms = 100 should return max for each",
  max_sms: 100,
  gsm7: 1600,
  unicode: 1600
}

describe("char counter", function() {
  runCountTest(emptySms);
  runCountTest(gsm7With2SizedChars);
  runCountTest(gsm7Without2SizedChars);
  runCountTest(unicodeSms);
  runMaxCharTest(emptyCount);
  runMaxCharTest(oneSmsCount);
  runMaxCharTest(tenCount);
  runMaxCharTest(overSizedCount);

  it('sould be sendable sms', function() {
    expect(dueSmsCounter.canBeSent(unicodeSms.string, 1)).toEqual(true);
  });

  it('2 sized gsm7 sms sould be send if size >= 2', function() {
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 1)).toEqual(false);
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 2)).toEqual(true);
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 4)).toEqual(true);
  });

  it('4 sized unicode sms sould be send if size >= 4', function() {
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 1)).toEqual(false);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 2)).toEqual(false);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 4)).toEqual(true);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 5)).toEqual(true);
  });

  it('2 sized gsm7 sms sould be send if size >= 2', function() {
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 1)).toEqual(false);
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 2)).toEqual(true);
    expect(dueSmsCounter.canBeSent(twoSizedGsm7Sms.string, 4)).toEqual(true);
  });

  it('4 sized unicode sms sould be send if size >= 4', function() {
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 1)).toEqual(false);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 2)).toEqual(false);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 4)).toEqual(true);
    expect(dueSmsCounter.canBeSent(fourSizedGsm7Sms.string, 5)).toEqual(true);
  });

  runSmsCountTest(emptySms);
  runSmsCountTest(gsm7With2SizedChars);
  runSmsCountTest(gsm7Without2SizedChars);
  runSmsCountTest(unicodeSms);
  runSmsCountTest(twoSizedGsm7Sms);
  runSmsCountTest(fourSizedGsm7Sms);

});