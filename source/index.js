import CONSTS from './consts.js';

const { B7_2SIZED_CHARS, B7_BASE_CHARS, B7_SMS_COUNT, UNICODE_SMS_COUNT} = CONSTS;
const B7_CHARS = [...B7_BASE_CHARS, ...B7_2SIZED_CHARS];

function uniq(array) {
  let uniq = []
  array.forEach(elem => {
    uniq.indexOf(elem) < 0 && uniq.push(elem)
  });
  return uniq;
}

function count_2chars_sized(sms) {
  let res = 0;
  sms.forEach(letter => {
    res += B7_2SIZED_CHARS.indexOf(letter) == -1 ? 0 : 1;
  });
  return res;
};

function calcSmsCount(char_count, is_unicode) {
  const base = is_unicode ? UNICODE_SMS_COUNT : B7_SMS_COUNT;
  let sms_count = 0;
  while(42) {
    if (!base[sms_count] || base[sms_count][1] >= char_count) { break }
    sms_count++
  }
  if (sms_count == 0)
    sms_count++
  return sms_count;
}

function calcCharAndSmsCount(sms) {
  const result = calcCharCount(sms);
  result.sms_count = calcSmsCount(result.char_count, result.is_unicode)
  return result;
}

function calcCharCount(sms) {
  sms = (sms || '').split('');
  const is_7_bits = uniq([...sms, ...B7_CHARS]).length == B7_CHARS.length;
  if (is_7_bits) {
    return {
      char_count: sms.length + count_2chars_sized(sms),
      is_unicode: false
    };
  } else {
    return {
      char_count: sms.length,
      is_unicode: true
    };
  }
}

function calcMaxCharCount(sms_count) {
  let gsm7    = B7_SMS_COUNT[sms_count.toString()];
  let unicode = UNICODE_SMS_COUNT[sms_count.toString()];
  gsm7    = (gsm7    === undefined && sms_count ? B7_SMS_COUNT['max']      : gsm7   )[1];
  unicode = (unicode === undefined && sms_count ? UNICODE_SMS_COUNT['max'] : unicode)[1];
  return {
    gsm7,
    unicode,
  }
}

function canBeSent(sms_body, max_sms) {
  const char_count = calcCharCount(sms_body),
        max_char   = calcMaxCharCount(max_sms),
        is_unicode = char_count.is_unicode
  ;

  return char_count.char_count <= max_char[is_unicode ? 'unicode' : 'gsm7'];
}

module.exports = {
  calcCharCount,
  calcMaxCharCount,
  canBeSent,
  calcSmsCount,
  calcCharAndSmsCount
}